package com.kalinin.credits.api;

import org.jivesoftware.smack.*;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;

/**
 * User: haribo
 * Date: 30.09.13
 * Time: 10:15
 */
public class SmackAPI implements MessageListener {

    private static final int packetReplyTimeout = 500; // millis

    private XMPPConnection connection;
    private ChatManager chatManager;
    private Chat chat;
    private String server;
    private int port;

    private Message recievedMessage = null;

    public SmackAPI(String server, int port) {
        this.server = server;
        this.port = port;
    }

    public void init() throws XMPPException {
        System.out.println(String.format("Initializing connection to server %1$s port %2$d", server, port));

        SmackConfiguration.setPacketReplyTimeout(packetReplyTimeout);
        SASLAuthentication.supportSASLMechanism("PLAIN");
//        SASLAuthentication.supportSASLMechanism("DIGEST-MD5", 0);
        ConnectionConfiguration config = new ConnectionConfiguration(server, port);
//        config.setSASLAuthenticationEnabled(false);
//        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

        connection = new XMPPConnection(config);
        connection.connect();

        System.out.println("Connected: " + connection.isConnected());

        chatManager = connection.getChatManager();
    }

    public void performLogin(String username, String password) throws XMPPException {
        if (connection != null && connection.isConnected()) {
            connection.login(username, password);
        }
    }

    @Override
    public void processMessage(Chat chat, Message message) {
        String from = message.getFrom();
        String body = message.getBody();
        System.out.println(String.format("Received message '%1$s' from %2$s", body, from));
        recievedMessage = message;
    }

    public void sendMessage(String message, String to) throws XMPPException {
        if (chat == null) {
            chat = chatManager.createChat(to, this);
        }
        chat.sendMessage(message);
    }

    public void destroy() {
        if (connection != null && connection.isConnected()) {
            connection.disconnect();
        }
    }

    public void setStatus(boolean available, String status) {

        Presence.Type type = available ? Presence.Type.available : Presence.Type.unavailable;
        Presence presence = new Presence(type);

        presence.setStatus(status);
        connection.sendPacket(presence);

    }

    public Message getRecievedMessage() {
        return recievedMessage;
    }

    public void setRecievedMessageNull() {
        recievedMessage = null;
    }
}
