package com.kalinin.credits.plugins;

import com.kalinin.credits.api.SmackAPI;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * User: haribo
 * Date: 30.09.13
 * Time: 10:12
 */
public class SmackJabber extends CordovaPlugin {

    private static final String SEND_MESSAGE = "sendMessage";
    private static final String LISTEN_MESSAGE = "listenMessage";
    private static final String CONNECT_AND_LOGIN = "connectAndLogin";
    private static final String DISCONNECT = "disconnect";

    private static final String SERVER = "server";
    private static final String PORT = "port";

    private static final String USERNAME = "username";
    private static final String PASSWD = "password";

    private static final String MESSAGE = "message";
    private static final String TO = "to";

    private CallbackContext cbContext;
    private SmackAPI smackAPI;

    private PluginResult res;

    public SmackJabber() {}

    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
        this.cbContext = callbackContext;

        JSONObject obj = args.optJSONObject(0);

        switch (action) {
            case CONNECT_AND_LOGIN:
                if (obj == null) {
                    return contextError("Unbelievable error. Please contact with administrator");
                }

                String server = obj.optString(SERVER);
                Integer port = obj.optInt(PORT);

                String username = obj.optString(USERNAME);
                String password = obj.optString(PASSWD);

                if (server == null || port == 0) {
                    return contextError("Specify server and port to connect!");
                }

                if (username == null || password == null) {
                    return contextError("User Credentials are wrong!");
                }

                smackAPI = new SmackAPI(server, port);
                try {
                    smackAPI.init();
                    smackAPI.performLogin(username, password);
                    cbContext.success("connect and login success");
                } catch (XMPPException e) {
                    e.printStackTrace();
                    smackAPI = null;
                    return contextError(e.toString());
                }
                break;
            case SEND_MESSAGE:
                if (obj == null) {
                    return contextError("Unbelievable error. Please contact with administrator");
                }
                String message = obj.optString(MESSAGE);
                String to = obj.optString(TO);

                if (smackAPI == null) {
                    return contextError("Connect to server first!");
                }

                try {
                    smackAPI.sendMessage(message, to);
                    cbContext.success("Message sent!");
                } catch (XMPPException e) {
                    return contextError(e.toString());
                }
                break;
            case LISTEN_MESSAGE:
                res = new PluginResult(PluginResult.Status.NO_RESULT);
                res.setKeepCallback(true);
                cordova.getThreadPool().execute(new Runnable() {
                    @Override
                    public void run() {
                        String callbackId = cbContext.getCallbackId();
                        while (true) {
                            String msg = getMsg();
                            if (msg != null) {
                                res = new PluginResult(PluginResult.Status.OK, msg);
                                res.setKeepCallback(true);
                                CallbackContext cb = new CallbackContext(callbackId, webView);
                                cb.sendPluginResult(res);
                                msg = null;
                            }
                        }
                    }
                });
                cbContext.sendPluginResult(res);
                return true;
            case DISCONNECT:
                if (smackAPI != null) {
                    smackAPI.destroy();
                    cbContext.success("disconnected");
                }
                break;
        }

        return true;
    }

    private String getMsg() {
        Message rMsg = smackAPI.getRecievedMessage();
        if (rMsg != null && rMsg.getBody() != null) {
            smackAPI.setRecievedMessageNull();
            return rMsg.getBody();
        }
        return null;
    }

    private boolean contextError(String error) {
        this.cbContext.error("Error: " + error);
        return false;
    }
}
