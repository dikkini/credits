package com.kalinin.credits.plugins;

import android.webkit.WebView;
import org.apache.cordova.DroidGap;

/**
 * Created with IntelliJ IDEA.
 * User: dikkini
 * Date: 10/9/13
 * Time: 5:48 PM
 */
public class JabberMessageListener {
    private WebView mAppView;
    private DroidGap mGap;

    public JabberMessageListener(DroidGap gap, WebView view) {
        mAppView = view;
        mGap = gap;
    }
}
