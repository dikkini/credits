/**
 * Created with IntelliJ IDEA.
 * User: haribo
 * Date: 26.09.13
 * Time: 13:35
 */

cordova.define("cordova/plugins/smackjabber",
    function(require, exports, module) {
        var exec = require("cordova/exec");
        var SmackJabber = function() {};

        //-------------------------------------------------------------------
        SmackJabber.prototype.connectAndLogin = function(server, port, username, password, successCallback, errorCallback) {
            if (errorCallback == null) { errorCallback = function() {}}

            if (typeof errorCallback != "function")  {
                console.log("SmackJabber.sendMessage failure: failure parameter not a function");
                return
            }

            if (typeof successCallback != "function") {
                console.log("SmackJabber.sendMessage failure: success callback parameter must be a function");
                return
            }

            exec(successCallback, errorCallback, 'SmackJabber', 'connectAndLogin',
                [{"server": server, "port" : port, "username" : username, "password" : password}]);
        };

        //-------------------------------------------------------------------
        SmackJabber.prototype.sendMessage = function(message, to, successCallback, errorCallback) {
            if (errorCallback == null) { errorCallback = function() {}}

            if (typeof errorCallback != "function")  {
                console.log("SmackJabber.sendMessage failure: failure parameter not a function");
                return
            }

            if (typeof successCallback != "function") {
                console.log("SmackJabber.sendMessage failure: success callback parameter must be a function");
                return
            }

            exec(successCallback, errorCallback, 'SmackJabber', 'sendMessage',
                [{"message" : message, "to" : to}]);
        };

        //-------------------------------------------------------------------
        SmackJabber.prototype.listenMessage = function(successCallback, errorCallback) {
            if (errorCallback == null) { errorCallback = function() {}}

            if (typeof errorCallback != "function")  {
                console.log("SmackJabber.listenMessage failure: failure parameter not a function");
                return
            }

            if (typeof successCallback != "function") {
                console.log("SmackJabber.listenMessage failure: success callback parameter must be a function");
                return
            }

            exec(successCallback, errorCallback, 'SmackJabber', 'listenMessage', []);
        };

        var smackJabber = new SmackJabber();
        module.exports = smackJabber;

    });
//-------------------------------------------------------------------

if(!window.plugins) {
    window.plugins = {};
}
if (!window.plugins.smackJabber) {
    window.plugins.smackJabber = cordova.require("cordova/plugins/smackjabber");
}
